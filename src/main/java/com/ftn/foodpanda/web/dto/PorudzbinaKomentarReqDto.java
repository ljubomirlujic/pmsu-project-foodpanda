package com.ftn.foodpanda.web.dto;

import lombok.Data;

@Data
public class PorudzbinaKomentarReqDto {

    private String komentar;
}
