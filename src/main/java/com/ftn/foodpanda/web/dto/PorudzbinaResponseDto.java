package com.ftn.foodpanda.web.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PorudzbinaResponseDto {

    private String id;

    private String komentar;

    private int ocena;

    private LocalDateTime satnica;

    private boolean anonimanKomentar;
}
