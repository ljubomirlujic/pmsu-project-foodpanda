package com.ftn.foodpanda.web.dto;

import lombok.*;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Builder
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ArtikalDto {

    private Long Id;
    @NotBlank(message = "name can't be blank")
    @Length(max = 40)
    private String naziv;
    @Length(max = 200)
    @NotBlank(message = "description can't be blank")
    private String opis;
    @Min(0L)
    private double cena;
    @NotBlank
    private String putanjaSlike;

    private Long prodavacId;
}
