package com.ftn.foodpanda.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class ProdavacRegisterDto {

    private LocalDate poslujeOd;
    @NotBlank
    private String email;
    @NotBlank
    private String adresa;
    @NotBlank
    private String naziv;
    @NotNull
    private KorisnikRegisterDto korisnik;

    public ProdavacRegisterDto() {
    }


    public LocalDate getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(LocalDate poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public KorisnikRegisterDto getKorisnik() {
        return korisnik;
    }
    public void setKorisnik(KorisnikRegisterDto korisnik) {
        this.korisnik = korisnik;
    }
}
