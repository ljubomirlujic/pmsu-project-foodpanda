package com.ftn.foodpanda.web.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class KorisnikDto {

    private Long id;
    @NotBlank
    private String ime;
    @NotBlank
    private String prezime;
    @NotBlank
    private String username;

    private boolean blokiran;

    public KorisnikDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isBlokiran() {
        return blokiran;
    }

    public void setBlokiran(boolean blokiran) {
        this.blokiran = blokiran;
    }
}
