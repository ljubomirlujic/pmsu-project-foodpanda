package com.ftn.foodpanda.web.dto;

import javax.validation.constraints.NotBlank;

public class JwtAuthenticationRequest {
    @NotBlank
    private String username;
    @NotBlank
    private String password;

    public JwtAuthenticationRequest(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public JwtAuthenticationRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
