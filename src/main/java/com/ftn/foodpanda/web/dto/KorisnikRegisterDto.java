package com.ftn.foodpanda.web.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class KorisnikRegisterDto extends KorisnikDto{
    @NotBlank
    @Length(min = 4)
    private String password;


    public KorisnikRegisterDto() {
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
