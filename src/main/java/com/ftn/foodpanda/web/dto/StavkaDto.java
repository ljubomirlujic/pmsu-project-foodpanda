package com.ftn.foodpanda.web.dto;

public class StavkaDto {

    private Long id;

    private int kolicina;

    private ArtikalPorudzbinaDto artikal;

    public StavkaDto() {
    }

    public StavkaDto(Long id, int kolicina, ArtikalPorudzbinaDto artikal) {
        this.id = id;
        this.kolicina = kolicina;
        this.artikal = artikal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public ArtikalPorudzbinaDto getArtikal() {
        return artikal;
    }

    public void setArtikal(ArtikalPorudzbinaDto artikal) {
        this.artikal = artikal;
    }
}
