package com.ftn.foodpanda.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class ProdavacDto {

    private Long id;

    private LocalDate poslujeOd;
    @NotBlank
    private String email;
    @NotBlank
    private String adresa;
    @NotBlank
    private String naziv;
    @NotNull
    private KorisnikDto korisnik;

    public ProdavacDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(LocalDate poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public KorisnikDto getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(KorisnikDto korisnik) {
        this.korisnik = korisnik;
    }

}
