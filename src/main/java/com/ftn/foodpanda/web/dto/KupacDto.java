package com.ftn.foodpanda.web.dto;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class KupacDto {

    private Long id;
    @NotBlank
    private String adresa;
    @NotNull
    private KorisnikDto korisnik;


    public KupacDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public KorisnikDto getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(KorisnikDto korisnik) {
        this.korisnik = korisnik;
    }
}
