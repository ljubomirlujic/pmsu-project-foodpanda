package com.ftn.foodpanda.web.dto.mapper;

import com.ftn.foodpanda.esModel.ArtikalEs;
import com.ftn.foodpanda.esModel.PorudzbinaEs;
import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.web.dto.*;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;

public class PorudzbinaMapper {

    public static PorudzbinaEs mapPorudzbinaESModel(PorudzbinaOcjenjivanjeDto dto){
        return PorudzbinaEs.builder()
                .komentar(dto.getKomentar())
                .ocena(dto.getOcena())
                .satnica(dto.getSatnica())
                .anonimanKomentar(dto.isAnonimanKomentar())
                .build();
    }

    public static PorudzbinaResponseDto mapResponseDto(PorudzbinaEs porudzbina){
        return PorudzbinaResponseDto.builder()
                .id(porudzbina.getId())
                .komentar(porudzbina.getKomentar())
                .ocena(porudzbina.getOcena())
                .anonimanKomentar(porudzbina.isAnonimanKomentar())
                .satnica(porudzbina.getSatnica())
                .build();
    }
    public static List<PorudzbinaResponseDto> mapDtos(SearchHits<PorudzbinaEs> searchHits) {
        return searchHits
                .map(porudzbina -> mapResponseDto(porudzbina.getContent()))
                .toList();
    }

}
