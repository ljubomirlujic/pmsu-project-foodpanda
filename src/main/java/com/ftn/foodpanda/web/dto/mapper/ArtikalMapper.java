package com.ftn.foodpanda.web.dto.mapper;

import com.ftn.foodpanda.esModel.ArtikalEs;
import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalResponseDto;
import org.springframework.data.elasticsearch.core.SearchHits;

import java.util.List;

public class ArtikalMapper {

    public static ArtikalEs mapESModel(ArtikalDto artikalDto){
        return ArtikalEs.builder()
                .naziv(artikalDto.getNaziv())
                .opis(artikalDto.getOpis())
                .cena(artikalDto.getCena())
                .build();

    }

    public static Artikal mapModel(ArtikalDto artikalDto){
        return Artikal.builder()
                .naziv(artikalDto.getNaziv())
                .opis(artikalDto.getOpis())
                .cena(artikalDto.getCena())
                .putanjaSlike(artikalDto.getPutanjaSlike())
                .prodavacId(artikalDto.getProdavacId())
                .build();

    }
    public static ArtikalResponseDto mapResponseDto(ArtikalEs artikal){
        return ArtikalResponseDto.builder()
                .id(artikal.getId())
                .naziv(artikal.getNaziv())
                .opis(artikal.getOpis())
                .cena(artikal.getCena())
                .prosjecnaOcjena(artikal.getProsjecnaOcjena())
                .brojKomentara(artikal.getBrojKomentara())
                .build();
    }

    public static List<ArtikalResponseDto> mapDtos(SearchHits<ArtikalEs> searchHits) {
        return searchHits
                .map(artikal -> mapResponseDto(artikal.getContent()))
                .toList();
    }

}
