package com.ftn.foodpanda.web.dto;

import lombok.Data;

@Data
public class ArtikalOpisRequestDto {

    private String opis;
}
