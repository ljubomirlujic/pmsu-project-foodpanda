package com.ftn.foodpanda.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class KupacRegisterDto {

    @NotBlank
    private String adresa;
    @NotNull
    private KorisnikRegisterDto korisnik;

    public KupacRegisterDto() {
    }


    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public KorisnikRegisterDto getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(KorisnikRegisterDto korisnik) {
        this.korisnik = korisnik;
    }
}

