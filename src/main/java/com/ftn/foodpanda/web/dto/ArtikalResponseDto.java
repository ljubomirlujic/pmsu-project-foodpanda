package com.ftn.foodpanda.web.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArtikalResponseDto {

    private String id;

    private String naziv;

    private String opis;

    private double cena;

    private double prosjecnaOcjena;

    private int brojKomentara;
}
