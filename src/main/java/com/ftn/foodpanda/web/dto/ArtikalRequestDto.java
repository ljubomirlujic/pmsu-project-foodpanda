package com.ftn.foodpanda.web.dto;

import lombok.Builder;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


@Data
@Builder
public class ArtikalRequestDto {
    
    private String naziv;

    private String opis;

    private double cena;

    private String putanjaSlike;

    private Long prodavacId;
}
