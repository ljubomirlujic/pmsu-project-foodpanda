package com.ftn.foodpanda.web.dto;

import com.ftn.foodpanda.model.Stavka;
import com.sun.istack.Nullable;
import lombok.Builder;
import org.springframework.boot.context.properties.bind.DefaultValue;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Builder
public class PorudzbinaDto {

    private Long id;

    private LocalDateTime satnica;
    @NotNull
    private boolean dostavljeno;

    private int ocena;

    private String komentar;
    @NotNull
    private boolean anonimanKomentar;
    @NotNull
    private boolean arhiviranKomentar;

    private KupacDto kupac;

    private List<StavkaDto> stavke = new ArrayList<>();

    public PorudzbinaDto() {
    }

    public PorudzbinaDto(Long id, LocalDateTime satnica, boolean dostavljeno, int ocena, String komentar,
                         boolean anonimanKomentar, boolean arhiviranKomentar, KupacDto kupac, List<StavkaDto> stavke) {
        this.id = id;
        this.satnica = satnica;
        this.dostavljeno = dostavljeno;
        this.komentar = komentar;
        this.anonimanKomentar = anonimanKomentar;
        this.arhiviranKomentar = arhiviranKomentar;
        this.kupac = kupac;
        this.stavke = stavke;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getSatnica() {
        return satnica;
    }

    public void setSatnica(LocalDateTime satnica) {
        this.satnica = satnica;
    }

    public boolean isDostavljeno() {
        return dostavljeno;
    }

    public void setDostavljeno(boolean dostavljeno) {
        this.dostavljeno = dostavljeno;
    }


    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public boolean isAnonimanKomentar() {
        return anonimanKomentar;
    }

    public void setAnonimanKomentar(boolean anonimanKomentar) {
        this.anonimanKomentar = anonimanKomentar;
    }

    public boolean isArhiviranKomentar() {
        return arhiviranKomentar;
    }

    public void setArhiviranKomentar(boolean arhiviranKomentar) {
        this.arhiviranKomentar = arhiviranKomentar;
    }

    public List<StavkaDto> getStavke() {
        return stavke;
    }

    public void setStavke(List<StavkaDto> stavke) {
        this.stavke = stavke;
    }

    public KupacDto getKupac() {
        return kupac;
    }

    public void setKupac(KupacDto kupac) {
        this.kupac = kupac;
    }

    public int getOcena() {
        return ocena;
    }

    public void setOcena(int ocena) {
        this.ocena = ocena;
    }
}
