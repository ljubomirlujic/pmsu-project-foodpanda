package com.ftn.foodpanda.web.controller;


import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.service.ArtikalService;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.service.ProdavacService;
import com.ftn.foodpanda.support.ArtikalDtoToArtikal;
import com.ftn.foodpanda.support.ArtikalToArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalOpisRequestDto;
import com.ftn.foodpanda.web.dto.ArtikalResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/articles")
public class ArtikalController {

    private final ArtikalService artikalService;

    private final ArtikalToArtikalDto toDto;

    private final ArtikalDtoToArtikal toEntity;

    private final ProdavacService prodavacService;

    private final KorisnikService korisnikService;

    public ArtikalController(ArtikalService artikalService, ArtikalToArtikalDto toDto, ArtikalDtoToArtikal toEntity, ProdavacService prodavacService, KorisnikService korisnikService) {
        this.artikalService = artikalService;
        this.toDto = toDto;
        this.toEntity = toEntity;
        this.prodavacService = prodavacService;
        this.korisnikService = korisnikService;
    }

    @PermitAll
    @GetMapping
    public ResponseEntity<List<Artikal>> getAll(){
        List<Artikal> artikli = this.artikalService.findAll();
        return new ResponseEntity(this.toDto.convert(artikli), HttpStatus.OK);
    }
    @PreAuthorize("hasAnyRole('KUPAC','PRODAVAC')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Artikal> getOne(@PathVariable(name = "id") Long id){
        Optional<Artikal> artikal = this.artikalService.findOne(id);
        return  !artikal.isPresent() ? new ResponseEntity(HttpStatus.NOT_FOUND):
                new ResponseEntity(this.toDto.convert((Artikal)artikal.get()), HttpStatus.OK);

    }

    @PermitAll
    @GetMapping(value = "/pretraga")
    public ResponseEntity<List<ArtikalResponseDto>> getByNazivAndOpisAndPrice(@RequestParam(name = "naziv") String naziv,
                                                @RequestParam(name = "opis") String opis,
                                                @RequestParam(name = "odCena", defaultValue = "0") double odCena,
                                                @RequestParam(name = "doCena",  defaultValue = "9999999") double doCena,
                                                @RequestParam(name = "operator") String operator){

        if(operator.trim().equalsIgnoreCase("and")) {
            return new ResponseEntity(artikalService.findByNazivAndOpisAndCena(naziv, opis, odCena,doCena), HttpStatus.OK);
        }else if(operator.trim().equalsIgnoreCase("or")){
            return new ResponseEntity(artikalService.findByNazivOrOpisOrCena(naziv, opis, odCena,doCena), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PermitAll
    @GetMapping(value = "/pretraga/naziv")
    public List<ArtikalResponseDto> getByNaziv(@RequestParam(name = "naziv") String naziv) {
        return artikalService.findByNaziv(naziv);
    }

    @PermitAll
    @GetMapping(value = "/pretraga/opis")
    public ResponseEntity<List<ArtikalResponseDto>> getByOpis(@RequestParam(name = "text") String opis) {
        return new ResponseEntity(artikalService.findByOpis(opis), HttpStatus.OK);
    }
    @PermitAll
    @GetMapping("/pretraga/cena")
    public ResponseEntity<List<ArtikalResponseDto>> getByPriceRange(@RequestParam(name = "odCena", defaultValue = "0") double odCena,
                                                                    @RequestParam(name = "doCena",  defaultValue = "9999999") double doCena) {
        return new ResponseEntity(artikalService.findByCena(odCena, doCena), HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('KUPAC', 'PRODAVAC')")
    @GetMapping(value = "prodavac/{prodavac}")
    public ResponseEntity<List<Artikal>> findAllByProdavac(@PathVariable(name = "prodavac") String prodavac){
        Korisnik korisnik = korisnikService.findByUsername(prodavac);
        if(korisnik != null){
            List<Artikal> artikli = this.artikalService.findAllByProdavacId(korisnik.getId());
            return new ResponseEntity(this.toDto.convert(artikli), HttpStatus.OK);
        }else {
            Prodavac prod = prodavacService.findOneByNaziv(prodavac);
            List<Artikal> artikli = this.artikalService.findAllByProdavacId(prod.getId());
            return new ResponseEntity(this.toDto.convert(artikli), HttpStatus.OK);
        }

    }


//    @PreAuthorize("hasRole('PRODAVAC')")
    @PermitAll
    @PostMapping
    public ResponseEntity<Artikal> createArtikal(Authentication authentication, @Validated @RequestBody ArtikalDto artikalDto) {
        Long id = korisnikService.getLoggedIn(authentication).getId();
        artikalDto.setProdavacId(id);
        ArtikalDto saved = this.artikalService.save(artikalDto);
        return new ResponseEntity(saved, HttpStatus.CREATED);
    }
    @PreAuthorize("hasRole('PRODAVAC')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<Artikal> editArtikal(@PathVariable Long id,@Validated @RequestBody ArtikalDto reqBody){
        Optional<Artikal> artikal = this.artikalService.findOne(id);
        if(!artikal.isPresent()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }else {
//            Artikal toEdit = this.toEntity.convert(reqBody);
//            ArtikalDto respArtikalDto = this.artikalService.save(reqBody);
            ArtikalDto respArtikalDto = artikalService.update(id, reqBody);
            return new ResponseEntity(respArtikalDto, HttpStatus.OK);
        }
    }
    @PreAuthorize("hasRole('PRODAVAC')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteArtikal(@PathVariable(name = "id") Long id) {
        Artikal deleted = this.artikalService.delete(id);
        return deleted == null ? new ResponseEntity(HttpStatus.NOT_FOUND) :
                new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
