package com.ftn.foodpanda.web.controller;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.support.KorisnikDtoToKorisnik;
import com.ftn.foodpanda.support.KorisnikToKorisnikDto;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.ChangePasswordDto;
import com.ftn.foodpanda.web.dto.KorisnikDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/korisnici")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KorisnikToKorisnikDto toDto;

    @Autowired
    private KorisnikDtoToKorisnik toEntity;


    @GetMapping
    public  ResponseEntity<List<KorisnikDto>> getAll(){

        List<Korisnik> korisnici = korisnikService.findAll();

        return new ResponseEntity(toDto.convert(korisnici), HttpStatus.OK);
    }

    @GetMapping("/{username}")
    public ResponseEntity<KorisnikDto> getOneByUsername(@PathVariable String username){

        Korisnik korisnik = korisnikService.findByUsername(username);
        if(korisnik != null) {
            return new ResponseEntity(toDto.convert(korisnik), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
    @PreAuthorize("hasAnyRole('KUPAC', 'PRODAVAC','ADMINISTRATOR')")
    @PutMapping("/{id}")
    public ResponseEntity<KorisnikDto> update(@PathVariable Long id,@Validated @RequestBody KorisnikDto dto){
        Korisnik korisnik = korisnikService.findOne(id).orElse(null);
        if(korisnik == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else{
            dto.setId(id);
            Korisnik toEdit = this.toEntity.convert(dto);
            Korisnik presisted = this.korisnikService.save(toEdit);
            KorisnikDto respBody = this.toDto.convert(presisted);

            return new ResponseEntity(respBody,HttpStatus.OK);
        }

    }


    @PreAuthorize("hasAnyRole('KUPAC', 'PRODAVAC','ADMINISTRATOR')")
    @PostMapping("/password")
    public ResponseEntity changePassword(@Validated @RequestBody ChangePasswordDto dto){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        boolean changed = korisnikService.changePassword(username, dto.getOldPassword(), dto.getNewPassword());

        if(changed) {
            return new ResponseEntity(HttpStatus.OK);
        }
        else {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }


}
