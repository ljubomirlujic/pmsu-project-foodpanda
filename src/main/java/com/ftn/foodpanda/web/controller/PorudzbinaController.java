package com.ftn.foodpanda.web.controller;


import com.ftn.foodpanda.model.*;
import com.ftn.foodpanda.service.*;
import com.ftn.foodpanda.support.PorudzbinaDtoToPorudzbina;
import com.ftn.foodpanda.support.PorudzbinaOcjenjivanjeDtoToEntity;
import com.ftn.foodpanda.support.PorudzbinaToPorudzbinaOcjenjivanjeDto;
import com.ftn.foodpanda.support.PorudzbinaToPorudzbinaDto;
import com.ftn.foodpanda.web.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/porudzbine")
public class PorudzbinaController {

    @Autowired
    private PorudzbinaService porudzbinaService;

    @Autowired
    private StavkaService stavkaService;

    @Autowired
    private PorudzbinaDtoToPorudzbina toEntity;

    @Autowired
    private PorudzbinaOcjenjivanjeDtoToEntity ocjenaToEntity;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KupacService kupacService;

    @Autowired
    private PorudzbinaToPorudzbinaDto toPorudzbinaDto;

    @Autowired
    private PorudzbinaToPorudzbinaOcjenjivanjeDto toPorudzbinaOcjenjivanjeDto;


    @GetMapping
    public ResponseEntity<List<PorudzbinaDto>> getAll(){

        List<Porudzbina> porudzbine = porudzbinaService.findAll();

        return new ResponseEntity(toPorudzbinaDto.convert(porudzbine), HttpStatus.OK);
    }

    @GetMapping("/kupac")
    public ResponseEntity<List<PorudzbinaDto>> getAllByKupac(){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Korisnik korisnik = korisnikService.findByUsername(username);
        Long id = korisnik.getId();

        List<Porudzbina> porudzbine = porudzbinaService.findAllByKupac(id);

        return new ResponseEntity(toPorudzbinaDto.convert(porudzbine), HttpStatus.OK);
    }

    @GetMapping("/prodavac")
    public ResponseEntity<List<PorudzbinaDto>> getAllByLoggedProdavac(){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Korisnik korisnik = korisnikService.findByUsername(username);
        Long id = korisnik.getId();

        List<Porudzbina> porudzbine = porudzbinaService.getAllByProdavac(id);

        return new ResponseEntity(toPorudzbinaDto.convert(porudzbine), HttpStatus.OK);
    }

    @GetMapping("/prodavac/{id}")
    public ResponseEntity<List<PorudzbinaDto>> getAllByProdavac(@PathVariable(name = "id") Long id){

        List<Porudzbina> porudzbine = porudzbinaService.getAllByProdavac(id);

        return new ResponseEntity(toPorudzbinaDto.convert(porudzbine), HttpStatus.OK);
    }

    @GetMapping("/pretraga/komentar")
    public ResponseEntity<List<PorudzbinaResponseDto>> getAllByKomentar(@RequestParam("text") String komentar){
        List<PorudzbinaResponseDto> porudzbine = porudzbinaService.findAllByKomentar(komentar);
        return new ResponseEntity(porudzbine, HttpStatus.OK);
    }

    @PermitAll
    @GetMapping("/pretraga/ocena")
    public ResponseEntity<List<ArtikalResponseDto>> getByPriceRange(@RequestParam(name = "odOcena", defaultValue = "1") int odOcena,
                                                                    @RequestParam(name = "doOcena",  defaultValue = "5") int doOcena) {
        return new ResponseEntity(porudzbinaService.findByOcena(odOcena, doOcena), HttpStatus.OK);
    }

    @PermitAll
    @GetMapping("/pretraga")
    public ResponseEntity<List<ArtikalResponseDto>> getByKomentarAndOcena(@RequestParam("operator") String operator,
                                                                    @RequestParam("text") String komentar,
                                                                    @RequestParam(name = "odOcena", defaultValue = "1") int odOcena,
                                                                    @RequestParam(name = "doOcena",  defaultValue = "5") int doOcena) {

        if(operator.trim().equalsIgnoreCase("and")) {
            return new ResponseEntity(porudzbinaService.findByKomentarAndOcena(komentar, odOcena, doOcena), HttpStatus.OK);
        }else if(operator.trim().equalsIgnoreCase("or")){
            return new ResponseEntity(porudzbinaService.findByKomentarOrOcena(komentar, odOcena, doOcena), HttpStatus.OK);
        }
        else{
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }



    @PostMapping
    public ResponseEntity<PorudzbinaDto> create(@Validated  @RequestBody PorudzbinaDto porudzbinaDto){

        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Korisnik korisnik = korisnikService.findByUsername(username);
        Long id = korisnik.getId();
        Kupac kupac = kupacService.findOne(id).orElse(null);

        Porudzbina porudzbina = toEntity.convert(porudzbinaDto);
        porudzbina.setKupac(kupac);
        porudzbina.setSatnica(LocalDateTime.now());

        Porudzbina presisted = porudzbinaService.save(porudzbina);

        for(Stavka s : porudzbina.getStavke()){
            s.setPorudzbina(presisted);
            stavkaService.save(s);
        }

        return new ResponseEntity(toPorudzbinaDto.convert(presisted), HttpStatus.CREATED);

    }

    @PreAuthorize("hasAnyRole('KUPAC', 'PRODAVAC')")
    @PutMapping(value = "/{id}")
    public ResponseEntity<PorudzbinaDto> editPorudzbina(@PathVariable Long id,@Validated @RequestBody PorudzbinaDto reqBody){
        Optional<Porudzbina> porudzbina = this.porudzbinaService.findOne(id);
        if(!porudzbina.isPresent()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }else {
//            reqBody.setId(id);
//            Porudzbina toEdit = this.toEntity.convert(reqBody);
//            Porudzbina presisted = this.porudzbinaService.save(toEdit);
            PorudzbinaDto respBody = porudzbinaService.update(id, reqBody);
            return new ResponseEntity(respBody, HttpStatus.OK);
        }
    }

    @PreAuthorize("hasRole('KUPAC')")
    @PutMapping(value = "/ocjena/{id}")
    public ResponseEntity<PorudzbinaDto> editPorudzbinaOcjenjivanje(@PathVariable Long id,
                                                              @Validated @RequestBody PorudzbinaOcjenjivanjeDto reqBody){
        Optional<Porudzbina> porudzbina = this.porudzbinaService.findOne(id);
        if(!porudzbina.isPresent()){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }else {
//            reqBody.setId(id);
//            Porudzbina toEdit = this.ocjenaToEntity.convert(reqBody);
            PorudzbinaDto respBody = this.porudzbinaService.ocjenjivanjeUpdate(id, reqBody);
            return new ResponseEntity(respBody, HttpStatus.OK);
        }
    }
}
