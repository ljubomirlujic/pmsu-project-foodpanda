package com.ftn.foodpanda.web.controller;

import com.ftn.foodpanda.model.Stavka;
import com.ftn.foodpanda.service.StavkaService;
import com.ftn.foodpanda.support.StavkeToDto;
import com.ftn.foodpanda.web.dto.StavkaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/stavke")
public class StavkaController {

    @Autowired
    private StavkaService stavkaService;

    @Autowired
    private StavkeToDto toDto;

    @GetMapping
    public ResponseEntity<List<StavkaDto>> getAll(){

        List<Stavka> stavke = stavkaService.findAll();

        return new ResponseEntity(toDto.convert(stavke), HttpStatus.OK);
    }
}
