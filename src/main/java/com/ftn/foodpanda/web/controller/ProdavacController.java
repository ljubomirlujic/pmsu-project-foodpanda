package com.ftn.foodpanda.web.controller;

import com.ftn.foodpanda.enumerations.KorisnickaUloga;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.model.Kupac;
import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.service.ProdavacService;
import com.ftn.foodpanda.support.KupacToKupacDto;
import com.ftn.foodpanda.support.ProdavacToProdavacDto;
import com.ftn.foodpanda.support.ProdavacToProdavacOcjenaDto;
import com.ftn.foodpanda.web.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import java.util.List;

@RestController
@RequestMapping(value = "/prodavci")
public class ProdavacController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ProdavacService prodavacService;


    @Autowired
    private ProdavacToProdavacDto toDto;

    @Autowired
    private ProdavacToProdavacOcjenaDto toOcjenaDto;

    @PreAuthorize("hasRole('KUPAC')")
    @GetMapping
    public ResponseEntity<List<ProdavacDto>> getAll(){
        List<Prodavac> prodavci = this.prodavacService.findAll();
        List<ProdavacOcjenaDto> prodavacDto = toOcjenaDto.convert(prodavci);

        return new ResponseEntity(prodavacDto, HttpStatus.OK);
    }
    @PermitAll
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
    path = "/registracija")
    public ResponseEntity<ProdavacDto> createProdavac(@Validated @RequestBody ProdavacRegisterDto reqBody){

            List<Prodavac> prodavci = prodavacService.findAll();

            for(Prodavac p : prodavci){
                if (p.getEmail().equals(reqBody.getEmail())){
                    return new ResponseEntity(HttpStatus.BAD_REQUEST);
                }
            }

            Korisnik korisnik = new Korisnik(reqBody.getKorisnik().getIme(), reqBody.getKorisnik().getPrezime(),
                    reqBody.getKorisnik().getUsername(), passwordEncoder.encode(reqBody.getKorisnik().getPassword()),
                    KorisnickaUloga.PRODAVAC);

            Prodavac prodavac = new Prodavac(reqBody.getEmail(),reqBody.getAdresa(),
                    reqBody.getNaziv(),korisnik);



            if(korisnikService.save(korisnik) == null) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            korisnikService.save(korisnik);
            prodavacService.save(prodavac);

            return new ResponseEntity(toDto.convert(prodavac), HttpStatus.CREATED);

    }
}
