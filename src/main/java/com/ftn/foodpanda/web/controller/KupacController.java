package com.ftn.foodpanda.web.controller;

import com.ftn.foodpanda.enumerations.KorisnickaUloga;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.model.Kupac;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.service.KupacService;
import com.ftn.foodpanda.support.KupacToKupacDto;
import com.ftn.foodpanda.web.dto.KupacDto;
import com.ftn.foodpanda.web.dto.KupacRegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;

@RestController
@RequestMapping(value = "/kupci")
public class KupacController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KupacService kupacService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private KupacToKupacDto toDto;

    @PermitAll
    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
            path = "/registracija")
    public ResponseEntity<KupacDto> createKupac(@Validated @RequestBody KupacRegisterDto reqBody){

            Korisnik korisnik = new Korisnik(reqBody.getKorisnik().getIme(), reqBody.getKorisnik().getPrezime(),
                    reqBody.getKorisnik().getUsername(), passwordEncoder.encode(reqBody.getKorisnik().getPassword()),
                    KorisnickaUloga.KUPAC);

            Kupac kupac = new Kupac(reqBody.getAdresa(), korisnik);

            if(korisnikService.save(korisnik) == null) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            korisnikService.save(korisnik);
            kupacService.save(kupac);
            return new ResponseEntity(toDto.convert(kupac), HttpStatus.CREATED);
    }
}
