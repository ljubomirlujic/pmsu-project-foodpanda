package com.ftn.foodpanda.web.controller;

import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.security.TokenUtils;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.web.dto.JwtAuthenticationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;

@RestController
@RequestMapping(value = "/auth")
public class AuthenticationController {

    @Autowired
    private KorisnikService korisnikService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private TokenUtils tokenUtils;

    @PermitAll
    @PostMapping({"/login"})
    public ResponseEntity<String> login(@Validated @RequestBody JwtAuthenticationRequest dto) {

        try {
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(dto.getUsername(), dto.getPassword());
            Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
            SecurityContextHolder.getContext().setAuthentication(authentication);

            UserDetails userDetails = this.userDetailsService.loadUserByUsername(dto.getUsername());
            return ResponseEntity.ok(this.tokenUtils.generateToken(userDetails));

        }catch (BadCredentialsException ex){
            return ResponseEntity.status(404).build();
        }
        catch (DisabledException ex ){
            return ResponseEntity.status(403).build();
        }
    }

}
