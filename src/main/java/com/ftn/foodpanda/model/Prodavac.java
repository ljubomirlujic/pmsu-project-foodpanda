package com.ftn.foodpanda.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Prodavac {

    @Id
    private Long id;

    private LocalDate poslujeOd  = LocalDate.now();

    @Column(unique = true, nullable = false)
    private String email;

    private String adresa;

    private String naziv;


    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id")
    private Korisnik korisnik;

    public Prodavac() {
    }

    public Prodavac(String email, String adresa, String naziv, Korisnik korisnik) {
        this.email = email;
        this.adresa = adresa;
        this.naziv = naziv;
        this.korisnik = korisnik;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPoslujeOd() {
        return poslujeOd;
    }

    public void setPoslujeOd(LocalDate poslujeOd) {
        this.poslujeOd = poslujeOd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }
}
