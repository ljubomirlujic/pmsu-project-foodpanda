package com.ftn.foodpanda.model;


import com.ftn.foodpanda.enumerations.KorisnickaUloga;

import javax.persistence.*;

@Entity
public class Korisnik {

    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;

    private String ime;

    private String prezime;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private boolean blokiran;

    @Enumerated(EnumType.STRING)
    private KorisnickaUloga uloga;

    public Korisnik() {
    }

    public Korisnik(String ime, String prezime, String username, String password, KorisnickaUloga uloga) {
        this.ime = ime;
        this.prezime = prezime;
        this.username = username;
        this.password = password;
        this.blokiran = false;
        this.uloga = uloga;
    }

    public Korisnik(Long id, String ime, String prezime, String username, String password, boolean blokiran, KorisnickaUloga uloga) {
        this.id = id;
        this.ime = ime;
        this.prezime = prezime;
        this.username = username;
        this.password = password;
        this.blokiran = blokiran;
        this.uloga = uloga;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isBlokiran() {
        return blokiran;
    }

    public void setBlokiran(boolean blokiran) {
        this.blokiran = blokiran;
    }

    public KorisnickaUloga getUloga() {
        return uloga;
    }

    public void setUloga(KorisnickaUloga uloga) {
        this.uloga = uloga;
    }
}
