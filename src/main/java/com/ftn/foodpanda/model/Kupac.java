package com.ftn.foodpanda.model;

import javax.persistence.*;

@Entity
public class Kupac {

    @Id
    private Long id;

    private String adresa;

    @OneToOne(cascade = CascadeType.ALL)
    @MapsId
    @JoinColumn(name = "id")
    private Korisnik korisnik;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Kupac() {
    }

    public Kupac(String adresa, Korisnik korisnik) {
        this.adresa = adresa;
        this.korisnik = korisnik;
    }
}
