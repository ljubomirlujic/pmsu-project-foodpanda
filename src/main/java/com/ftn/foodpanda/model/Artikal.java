package com.ftn.foodpanda.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Artikal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long Id;

    @Column(nullable = false, unique = true)
    String naziv;

    String opis;

    double cena;

    String putanjaSlike;

    Long prodavacId;


}
