package com.ftn.foodpanda.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Porudzbina {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "porudzbina_id", unique = true,nullable = false)
    private Long id;

    private LocalDateTime satnica;

    private boolean dostavljeno;

    private int ocena;

    private String komentar;

    private boolean anonimanKomentar;

    private boolean arhiviranKomentar;

    @ManyToOne
    @JoinColumn(name = "kupac_id", referencedColumnName = "id", nullable = false)
    private Kupac kupac;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "porudzbina")
    private List<Stavka> stavke = new ArrayList<>();



}
