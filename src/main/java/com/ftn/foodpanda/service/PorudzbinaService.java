package com.ftn.foodpanda.service;


import com.ftn.foodpanda.esModel.PorudzbinaEs;
import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.web.dto.ArtikalResponseDto;
import com.ftn.foodpanda.web.dto.PorudzbinaDto;
import com.ftn.foodpanda.web.dto.PorudzbinaOcjenjivanjeDto;
import com.ftn.foodpanda.web.dto.PorudzbinaResponseDto;

import java.util.List;
import java.util.Optional;

public interface PorudzbinaService {

    List<Porudzbina> findAll();

    List<Porudzbina> findAllByKupac(Long id);

    List<Porudzbina> getAllByProdavac(Long id);

    double getProsjecnaOcjenaProdavca(Long id);

    Optional<Porudzbina> findOne(Long id);

    Porudzbina save(Porudzbina porudzbina);

    PorudzbinaDto update(Long id, PorudzbinaDto dto);

    PorudzbinaDto ocjenjivanjeUpdate(Long id, PorudzbinaOcjenjivanjeDto dto);

    void delete(Long id);

    void index(PorudzbinaOcjenjivanjeDto dto);

    List<PorudzbinaResponseDto> findAllByKomentar(String text);

    List<PorudzbinaResponseDto> findByOcena(int odOcena, int doOcena);

    List<PorudzbinaResponseDto> findByKomentarAndOcena(String komentar ,int odOcena, int doOcena);

    List<PorudzbinaResponseDto> findByKomentarOrOcena(String komentar ,int odOcena, int doOcena);
}
