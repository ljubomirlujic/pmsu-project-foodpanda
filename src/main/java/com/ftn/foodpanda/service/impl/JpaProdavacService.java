package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.repository.KupacRepository;
import com.ftn.foodpanda.repository.ProdavacRepository;
import com.ftn.foodpanda.service.PorudzbinaService;
import com.ftn.foodpanda.service.ProdavacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaProdavacService implements ProdavacService {

    @Autowired
    private ProdavacRepository prodavacRepository;



    @Override
    public List<Prodavac> findAll() {

        List<Prodavac> prodavci =  prodavacRepository.findAll();

        return prodavci;
    }

    @Override
    public Optional<Prodavac> findOne(Long id) {
        return Optional.empty();
    }

    @Override
    public Prodavac findOneByNaziv(String naziv) {
        return prodavacRepository.findFirstByNaziv(naziv);
    }

    @Override
    public Prodavac save(Prodavac prodavac) {
        return prodavacRepository.save(prodavac);
    }

    @Override
    public Prodavac delete(Long id) {
        return null;
    }
}
