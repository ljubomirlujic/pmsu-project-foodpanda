package com.ftn.foodpanda.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.transaction.Transactional;

import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private KorisnikService korisnikService;

    public UserDetailsServiceImpl() {
    }

    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Korisnik user = (Korisnik) this.korisnikService.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", username));
        }
        else if(user.isBlokiran()){
            throw new DisabledException(String.format("user is blocked"));
        }
        else {
            List<GrantedAuthority> grantedAuthorities = new ArrayList();
            String role = "ROLE_" + user.getUloga().toString();
            grantedAuthorities.add(new SimpleGrantedAuthority(role));
            return new org.springframework.security.core.userdetails.User(user.getUsername().trim(),
                    user.getPassword().trim(), grantedAuthorities);
        }
    }
}