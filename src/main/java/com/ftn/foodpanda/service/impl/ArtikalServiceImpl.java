package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.esModel.ArtikalEs;
import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.repository.ArtikalRepository;
import com.ftn.foodpanda.repository.ESArtikalRepository;
import com.ftn.foodpanda.service.ArtikalService;
import com.ftn.foodpanda.support.ArtikalToArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalResponseDto;
import com.ftn.foodpanda.web.dto.PorudzbinaResponseDto;
import com.ftn.foodpanda.web.dto.SimpleQueryEs;
import com.ftn.foodpanda.web.dto.mapper.ArtikalMapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ArtikalServiceImpl implements ArtikalService {


    private final ArtikalRepository artikalRepository;

    private final ArtikalToArtikalDto toDto;

    private final ESArtikalRepository esArtikalRepository;

    private final ElasticsearchRestTemplate _elasticsearchRestTemplate;

    public ArtikalServiceImpl(ArtikalRepository artikalRepository, ArtikalToArtikalDto toDto, ESArtikalRepository esArtikalRepository, ElasticsearchRestTemplate elasticsearchRestTemplate) {
        this.artikalRepository = artikalRepository;
        this.toDto = toDto;
        this.esArtikalRepository = esArtikalRepository;
        _elasticsearchRestTemplate = elasticsearchRestTemplate;
    }

    @Override
    public List<Artikal> findAll() {
        return artikalRepository.findAll();
    }

    @Override
    public List<Artikal> findAllByProdavacId(Long id) {
        return artikalRepository.findAllByProdavacId(id);
    }

    @Override
    public Optional<Artikal> findOne(Long id) {
        return artikalRepository.findById(id);
    }

    @Override
    public ArtikalDto save(ArtikalDto artikalDto) {
        Artikal presisted = artikalRepository.save(ArtikalMapper.mapModel(artikalDto));
        esArtikalRepository.save(ArtikalMapper.mapESModel(artikalDto));
        return toDto.convert(presisted);
    }

    @Override
    public ArtikalDto update(Long id, ArtikalDto artikalDto) {
        Artikal forUpdate = ArtikalMapper.mapModel(artikalDto);
        forUpdate.setProdavacId(id);
        Artikal updated = artikalRepository.save(forUpdate);

        return toDto.convert(updated);
    }

    @Override
    public Artikal delete(Long id) {
        Optional<Artikal> artikalOptional = this.artikalRepository.findById(id);
        if(artikalOptional.isPresent()) {
            Artikal artikal = artikalOptional.get();
            this.artikalRepository.deleteById(id);
            return artikal;
        }else {
            return null;
        }

    }

    @Override
    public List<ArtikalResponseDto> findByNaziv(String naziv) {
        List<ArtikalEs> artikli = esArtikalRepository.findAllByNaziv(naziv);

        return mapArtikalToArtikalResponseDto(artikli);
    }

    @Override
    public List<ArtikalResponseDto> findByOpis(String opis) {
        List<ArtikalEs> artikli = esArtikalRepository.findAllByOpis(opis);

        return mapArtikalToArtikalResponseDto(artikli);
    }

    @Override
    public List<ArtikalResponseDto> findByCena(double odCena, double doCena) {
        String range = odCena + "-" + doCena;
        QueryBuilder priceQuery = SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("cena", range));

        BoolQueryBuilder boolQueryPrice = QueryBuilders
                .boolQuery()
                .must(priceQuery);
        return ArtikalMapper.mapDtos(searchByBoolQuery(boolQueryPrice));
    }

    @Override
    public List<ArtikalResponseDto> findByNazivAndOpisAndCena(String naziv, String opis, double odCena, double doCena) {
        Map<String, QueryBuilder> mapa = returnQueries(naziv,opis,odCena,doCena);;

        BoolQueryBuilder boolQueryCenaAndNazivAndOpis = QueryBuilders
                .boolQuery()
                .must(mapa.get("naziv"))
                .must(mapa.get("opis"))
                .must(mapa.get("cena"));
        return ArtikalMapper.mapDtos(searchByBoolQuery(boolQueryCenaAndNazivAndOpis));
    }

    @Override
    public List<ArtikalResponseDto> findByNazivOrOpisOrCena(String naziv, String opis, double odCena, double doCena) {
        Map<String, QueryBuilder> mapa = returnQueries(naziv,opis,odCena,doCena);

        BoolQueryBuilder boolQueryCenaOrNazivOrOpis = QueryBuilders
                .boolQuery()
                .should(mapa.get("naziv"))
                .should(mapa.get("opis"))
                .should(mapa.get("cena"));
        return ArtikalMapper.mapDtos(searchByBoolQuery(boolQueryCenaOrNazivOrOpis));
    }

    public Map<String, QueryBuilder> returnQueries(String naziv, String opis, double odCena, double doCena){
        String range = odCena + "-" + doCena;

        QueryBuilder nazivQuery = SearchQueryGenerator.createMatchQueryBuilder(new SimpleQueryEs("naziv", naziv));
        QueryBuilder opisQuery = SearchQueryGenerator.createMatchQueryBuilder(new SimpleQueryEs("opis", opis));
        QueryBuilder priceQuery = SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("cena", range));

        Map<String, QueryBuilder> mapa = new HashMap<>();
        mapa.put("naziv", nazivQuery);
        mapa.put("opis", opisQuery);
        mapa.put("cena", priceQuery);

        return mapa;
    }


    private SearchHits<ArtikalEs> searchByBoolQuery(BoolQueryBuilder boolQueryBuilder) {
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .build();

        return _elasticsearchRestTemplate.search(searchQuery, ArtikalEs.class,  IndexCoordinates.of("artikli"));
    }


    private List<ArtikalResponseDto> mapArtikalToArtikalResponseDto(List<ArtikalEs> artikli) {
        List<ArtikalResponseDto> artikalDtos = new ArrayList<>();
        for (ArtikalEs artikal: artikli) {
            artikalDtos.add(ArtikalMapper.mapResponseDto(artikal));
        }
        return artikalDtos;
    }
}
