package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.web.dto.SimpleQueryEs;
import org.elasticsearch.index.query.QueryBuilder;

public class SearchQueryGenerator {

    public static QueryBuilder createRangeQueryBuilder(SimpleQueryEs simpleQueryEs) {
        return com.practice.demo.lucene.search.QueryBuilderCustom.buildQuery(com.practice.demo.util.SearchType.RANGE, simpleQueryEs.getField(), simpleQueryEs.getValue());
    }

    public static QueryBuilder createMatchQueryBuilder(SimpleQueryEs simpleQueryEs) {
        if(simpleQueryEs.getValue().startsWith("\"") && simpleQueryEs.getValue().endsWith("\"")) {
            return com.practice.demo.lucene.search.QueryBuilderCustom.buildQuery(com.practice.demo.util.SearchType.PHRASE, simpleQueryEs.getField(), simpleQueryEs.getValue());
        } else {
            return com.practice.demo.lucene.search.QueryBuilderCustom.buildQuery(com.practice.demo.util.SearchType.MATCH, simpleQueryEs.getField(), simpleQueryEs.getValue());
        }
    }

}
