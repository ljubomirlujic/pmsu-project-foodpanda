package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.repository.KorisnikRepository;
import com.ftn.foodpanda.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaKorisnikService implements KorisnikService {

    @Autowired
    private KorisnikRepository korisnikRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<Korisnik> findAll() {
        return korisnikRepository.findAll();
    }


    @Override
    public Optional<Korisnik> findOne(Long id) {
        return korisnikRepository.findById(id);
    }

    @Override
    public Korisnik getLoggedIn(Authentication authentication) {
        UserDetails userPrincipal = (UserDetails)authentication.getPrincipal();
        String username = userPrincipal.getUsername();

        Korisnik korisnik = korisnikRepository.findFirstByUsername(username);

        return korisnik;
    }

    @Override
    public Korisnik findByUsername(String username) {
        return korisnikRepository.findFirstByUsername(username);
    }

    @Override
    public boolean changePassword(String username, String oldPassword, String newPassword) {

        Korisnik korisnik = korisnikRepository.findFirstByUsername(username);

        if(passwordEncoder.matches(oldPassword,korisnik.getPassword())){
            korisnik.setPassword(passwordEncoder.encode(newPassword));
            save(korisnik);
            return true;
        }
        return false;
    }

    @Override
    public Korisnik save(Korisnik korisnik) {
        if(korisnik.getId() == null) {
            Korisnik kor = findByUsername(korisnik.getUsername());
            if (kor == null) {
                return korisnikRepository.save(korisnik);
            } else {
                return null;
            }
        }else{
            return korisnikRepository.save(korisnik);
        }
    }

    @Override
    public Korisnik delete(Long id) {
        return null;
    }
}
