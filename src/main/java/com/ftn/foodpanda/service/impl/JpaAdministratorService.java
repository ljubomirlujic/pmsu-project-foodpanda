package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.model.Administrator;
import com.ftn.foodpanda.service.AdministratorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaAdministratorService implements AdministratorService {
    @Override
    public List<Administrator> findAll() {
        return null;
    }

    @Override
    public Optional<Administrator> findOne(Long id) {
        return Optional.empty();
    }

    @Override
    public Administrator save(Administrator administrator) {
        return null;
    }

    @Override
    public Administrator delete(Long id) {
        return null;
    }
}
