package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.esModel.PorudzbinaEs;
import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.repository.ESPorudzbinaRepository;
import com.ftn.foodpanda.repository.PorudzbinaRepository;
import com.ftn.foodpanda.service.PorudzbinaService;
import com.ftn.foodpanda.support.PorudzbinaDtoToPorudzbina;
import com.ftn.foodpanda.support.PorudzbinaOcjenjivanjeDtoToEntity;
import com.ftn.foodpanda.support.PorudzbinaToPorudzbinaDto;
import com.ftn.foodpanda.web.dto.*;
import com.ftn.foodpanda.web.dto.mapper.ArtikalMapper;
import com.ftn.foodpanda.web.dto.mapper.PorudzbinaMapper;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class PorudzbinaServiceImpl implements PorudzbinaService {


    private final PorudzbinaRepository porudzbinaRepository;

    private final ESPorudzbinaRepository esPorudzbinaRepository;

    private final PorudzbinaOcjenjivanjeDtoToEntity ocjenaToEntity;

    private final ElasticsearchRestTemplate _elasticsearchRestTemplate;

    private final PorudzbinaToPorudzbinaDto toDto;

    private final PorudzbinaDtoToPorudzbina toEntity;

    public PorudzbinaServiceImpl(PorudzbinaRepository porudzbinaRepository, ESPorudzbinaRepository esPorudzbinaRepository, PorudzbinaOcjenjivanjeDtoToEntity ocjenaToEntity, ElasticsearchRestTemplate elasticsearchRestTemplate, PorudzbinaToPorudzbinaDto toDto, PorudzbinaDtoToPorudzbina toEntity) {
        this.porudzbinaRepository = porudzbinaRepository;
        this.esPorudzbinaRepository = esPorudzbinaRepository;
        this.ocjenaToEntity = ocjenaToEntity;
        _elasticsearchRestTemplate = elasticsearchRestTemplate;
        this.toDto = toDto;
        this.toEntity = toEntity;
    }

    @Override
    public List<Porudzbina> findAll() {
        return porudzbinaRepository.findAll();
    }

    @Override
    public List<Porudzbina> findAllByKupac(Long id) {
        List<Porudzbina> porudzbine = porudzbinaRepository.getAllByKupac(id);
        return porudzbine;
    }

    @Override
    public List<Porudzbina> getAllByProdavac(Long id) {
        List<Porudzbina> porudzbine = porudzbinaRepository.getAllByProdavac(id);
        return porudzbine;
    }

    @Override
    public double getProsjecnaOcjenaProdavca(Long id) {
        double o = porudzbinaRepository.prosjecnaOcjena(id);
        return o;
    }

    @Override
    public Optional<Porudzbina> findOne(Long id) {
        return porudzbinaRepository.findById(id);
    }

    @Override
    public Porudzbina save(Porudzbina porudzbina) {
        return porudzbinaRepository.save(porudzbina);
    }

    @Override
    public PorudzbinaDto update(Long id, PorudzbinaDto dto) {
        Porudzbina porudzbinaResp = toEntity.convert(dto);
        updatePorudzbina(id, porudzbinaResp);
        return toDto.convert(porudzbinaResp);
    }

    @Override
    public PorudzbinaDto ocjenjivanjeUpdate(Long id, PorudzbinaOcjenjivanjeDto dto) {
        Porudzbina porudzbinaResp = ocjenaToEntity.convert(dto);
        updatePorudzbina(id, porudzbinaResp);
        index(dto);
        return toDto.convert(porudzbinaResp);
    }

    @Override
    public void delete(Long id) { porudzbinaRepository.deleteById(id);
    }

    @Override
    public void index(PorudzbinaOcjenjivanjeDto dto) {
        esPorudzbinaRepository.save(PorudzbinaMapper.mapPorudzbinaESModel(dto));
    }

    @Override
    public List<PorudzbinaResponseDto> findAllByKomentar(String text) {
        List<PorudzbinaEs> porudzbine = esPorudzbinaRepository.findAllByKomentar(text);
        return mapPorudzbineToPorudzbineResponseDto(porudzbine);
    }

    @Override
    public List<PorudzbinaResponseDto> findByOcena(int odOcena, int doOcena) {
        String range = odOcena + "-" + doOcena;
        QueryBuilder priceQuery = SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("ocena", range));

        BoolQueryBuilder boolQueryPrice = QueryBuilders
                .boolQuery()
                .must(priceQuery);
        return PorudzbinaMapper.mapDtos(searchByBoolQuery(boolQueryPrice));
    }

    @Override
    public List<PorudzbinaResponseDto> findByKomentarAndOcena(String komentar, int odOcena, int doOcena) {
        Map<String, QueryBuilder> mapa = returnQueries(komentar, odOcena, doOcena);

        BoolQueryBuilder boolQueryOcenaAndKomentar = QueryBuilders
                .boolQuery()
                .must(mapa.get("komentar"))
                .must(mapa.get("ocena"));

        return PorudzbinaMapper.mapDtos(searchByBoolQuery(boolQueryOcenaAndKomentar));
    }

    @Override
    public List<PorudzbinaResponseDto> findByKomentarOrOcena(String komentar, int odOcena, int doOcena) {
        Map<String, QueryBuilder> mapa = returnQueries(komentar, odOcena, doOcena);

        BoolQueryBuilder boolQueryOcenaOrKomentar = QueryBuilders
                .boolQuery()
                .should(mapa.get("komentar"))
                .should(mapa.get("ocena"));

        return PorudzbinaMapper.mapDtos(searchByBoolQuery(boolQueryOcenaOrKomentar));
    }

    public Map<String, QueryBuilder> returnQueries(String komentar, int odOcena, int doOcena){
        String range = odOcena + "-" + doOcena;

        QueryBuilder ocenaQuery = SearchQueryGenerator.createRangeQueryBuilder(new SimpleQueryEs("ocena", range));
        QueryBuilder komentarQuery = SearchQueryGenerator.createMatchQueryBuilder(new SimpleQueryEs("komentar", komentar));

        Map<String, QueryBuilder> mapa = new HashMap<>();
        mapa.put("komentar", komentarQuery);
        mapa.put("ocena", ocenaQuery);

        return mapa;
    }

    private SearchHits<PorudzbinaEs> searchByBoolQuery(BoolQueryBuilder boolQueryBuilder) {
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(boolQueryBuilder)
                .build();

        return _elasticsearchRestTemplate.search(searchQuery, PorudzbinaEs.class,  IndexCoordinates.of("porudzbine"));
    }

    public void updatePorudzbina(Long id, Porudzbina porudzbina){
        Porudzbina existed = porudzbinaRepository.getOne(id);
        porudzbina.setId(id);
        porudzbina.setKupac(existed.getKupac());
        porudzbinaRepository.save(porudzbina);
    }

    private List<PorudzbinaResponseDto> mapPorudzbineToPorudzbineResponseDto(List<PorudzbinaEs> porudzbine) {
        List<PorudzbinaResponseDto> porudzbineDtos = new ArrayList<>();
        for (PorudzbinaEs porudzbina: porudzbine) {
            porudzbineDtos.add(PorudzbinaMapper.mapResponseDto(porudzbina));
        }
        return porudzbineDtos;
    }
}
