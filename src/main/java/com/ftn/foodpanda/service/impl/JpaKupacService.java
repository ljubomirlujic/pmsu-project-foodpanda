package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.model.Kupac;
import com.ftn.foodpanda.repository.KupacRepository;
import com.ftn.foodpanda.service.KupacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaKupacService implements KupacService {

    @Autowired
    private KupacRepository kupacRepository;

    @Override
    public List<Kupac> findAll() {
        return null;
    }

    @Override
    public Optional<Kupac> findOne(Long id) {
        return kupacRepository.findById(id);
    }

    @Override
    public Kupac save(Kupac kupac) {
        return kupacRepository.save(kupac);
    }

    @Override
    public Kupac delete(Long id) {
        return null;
    }
}
