package com.ftn.foodpanda.service.impl;

import com.ftn.foodpanda.model.Stavka;
import com.ftn.foodpanda.repository.StavkaRepository;
import com.ftn.foodpanda.service.StavkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class JpaStavkaService implements StavkaService {

    @Autowired
    private StavkaRepository stavkaRepository;

    @Override
    public List<Stavka> findAll() {
        return stavkaRepository.findAll();
    }

    @Override
    public Optional<Stavka> findOne(Long id) {
        return stavkaRepository.findById(id);
    }

    @Override
    public Stavka save(Stavka stavka) {
        return stavkaRepository.save(stavka);
    }

    @Override
    public void delete(Long id) {
        stavkaRepository.deleteById(id);
    }
}
