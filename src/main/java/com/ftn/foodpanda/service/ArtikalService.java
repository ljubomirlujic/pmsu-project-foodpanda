package com.ftn.foodpanda.service;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.ArtikalRequestDto;
import com.ftn.foodpanda.web.dto.ArtikalResponseDto;
import com.ftn.foodpanda.web.dto.PorudzbinaResponseDto;

import java.util.List;
import java.util.Optional;

public interface ArtikalService {

    List<Artikal> findAll();

    List<Artikal> findAllByProdavacId(Long id);

    Optional<Artikal> findOne(Long id);

    ArtikalDto save(ArtikalDto artikalDto);

    ArtikalDto update(Long id, ArtikalDto artikalDto);

    Artikal delete(Long id);

    List<ArtikalResponseDto> findByNaziv(String naziv);

    List<ArtikalResponseDto> findByOpis(String opis);

    List<ArtikalResponseDto> findByCena(double odCena, double doCena);

    List<ArtikalResponseDto> findByNazivAndOpisAndCena(String naziv ,String opis, double odCena, double doCena);

    List<ArtikalResponseDto> findByNazivOrOpisOrCena(String naziv ,String opis, double odCena, double doCena);

}
