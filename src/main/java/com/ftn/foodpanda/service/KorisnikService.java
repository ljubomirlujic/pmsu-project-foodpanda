package com.ftn.foodpanda.service;



import com.ftn.foodpanda.model.Korisnik;
import org.springframework.security.core.Authentication;

import java.util.List;
import java.util.Optional;

public interface KorisnikService {


    List<Korisnik> findAll();

    Optional<Korisnik> findOne(Long id);

    Korisnik getLoggedIn(Authentication authentication);

    Korisnik findByUsername(String username);

    boolean changePassword(String username,String oldPassword, String newPassword);

    Korisnik save(Korisnik korisnik);

    Korisnik delete(Long id);
}
