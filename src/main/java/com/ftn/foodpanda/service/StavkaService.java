package com.ftn.foodpanda.service;


import com.ftn.foodpanda.model.Stavka;

import java.util.List;
import java.util.Optional;

public interface StavkaService {

    List<Stavka> findAll();

    Optional<Stavka> findOne(Long id);

    Stavka save(Stavka stavka);

    void delete(Long id);
}
