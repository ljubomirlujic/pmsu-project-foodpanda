package com.ftn.foodpanda.service;


import com.ftn.foodpanda.model.Prodavac;

import java.util.List;
import java.util.Optional;

public interface ProdavacService {

    List<Prodavac> findAll();

    Optional<Prodavac> findOne(Long id);

    Prodavac findOneByNaziv(String naziv);

    Prodavac save(Prodavac prodavac);

    Prodavac delete(Long id);
}
