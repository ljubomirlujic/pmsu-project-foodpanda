package com.ftn.foodpanda.service;


import com.ftn.foodpanda.model.Kupac;

import java.util.List;
import java.util.Optional;

public interface KupacService {

    List<Kupac> findAll();

    Optional<Kupac> findOne(Long id);

    Kupac save(Kupac kupac);

    Kupac delete(Long id);
}
