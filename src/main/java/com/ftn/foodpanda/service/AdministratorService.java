package com.ftn.foodpanda.service;



import com.ftn.foodpanda.model.Administrator;

import java.util.List;
import java.util.Optional;

public interface AdministratorService {

    List<Administrator> findAll();

    Optional<Administrator> findOne(Long id);

    Administrator save(Administrator administrator);

    Administrator delete(Long id);
}
