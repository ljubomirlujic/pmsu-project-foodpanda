package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.service.ArtikalService;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;



@Component
public class ArtikalDtoToArtikal implements Converter<ArtikalDto, Artikal> {

    @Autowired
    private ArtikalService artikalService;

    public ArtikalDtoToArtikal() {
    }

    @Override
    public Artikal convert(ArtikalDto artikalDto) {
        Artikal artikal = null;
        if(artikalDto.getId() != null){
            artikal = (Artikal)this.artikalService.findOne(artikalDto.getId()).get();
        }

        if(artikal == null){
            artikal = new Artikal();
        }

        artikal.setId(artikalDto.getId());
        artikal.setNaziv(artikalDto.getNaziv());
        artikal.setOpis(artikalDto.getOpis());
        artikal.setCena(artikalDto.getCena());
        artikal.setPutanjaSlike(artikalDto.getPutanjaSlike());
        artikal.setProdavacId(artikalDto.getProdavacId());

        return artikal;
    }
}
