package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.service.PorudzbinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.web.dto.PorudzbinaDto;

import org.springframework.stereotype.Component;

@Component
public class PorudzbinaDtoToPorudzbina implements Converter<PorudzbinaDto, Porudzbina> {


    @Autowired
    private StavkeDtoToStavke stavkeDtoToStavke;

    @Override
    public Porudzbina convert(PorudzbinaDto porudzbinaDto) {


        Porudzbina porudzbina = new Porudzbina();


        porudzbina.setSatnica(porudzbinaDto.getSatnica());
        porudzbina.setDostavljeno(porudzbinaDto.isDostavljeno());
        porudzbina.setAnonimanKomentar(porudzbinaDto.isAnonimanKomentar());
        porudzbina.setArhiviranKomentar(porudzbinaDto.isArhiviranKomentar());
        porudzbina.setKomentar(porudzbinaDto.getKomentar());
//        porudzbina.setKupac(porudzbinaDto.getKupac());
        porudzbina.setStavke(stavkeDtoToStavke.convert(porudzbinaDto.getStavke()));

        return porudzbina;
    }
}
