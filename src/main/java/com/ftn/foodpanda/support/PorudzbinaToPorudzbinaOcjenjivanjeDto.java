package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.web.dto.PorudzbinaDto;
import com.ftn.foodpanda.web.dto.PorudzbinaOcjenjivanjeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class PorudzbinaToPorudzbinaOcjenjivanjeDto implements Converter<Porudzbina, PorudzbinaOcjenjivanjeDto> {

    @Autowired
    private StavkeToDto stavkeToDto;

    @Override
    public PorudzbinaOcjenjivanjeDto convert(Porudzbina porudzbina) {

        PorudzbinaOcjenjivanjeDto retVal = new PorudzbinaOcjenjivanjeDto();
        retVal.setId(porudzbina.getId());
        retVal.setAnonimanKomentar(porudzbina.isAnonimanKomentar());
        retVal.setDostavljeno(porudzbina.isDostavljeno());
        retVal.setKomentar(porudzbina.getKomentar());
        retVal.setArhiviranKomentar(porudzbina.isArhiviranKomentar());
        retVal.setSatnica(porudzbina.getSatnica());
        retVal.setOcena(porudzbina.getOcena());
        retVal.setStavke(stavkeToDto.convert(porudzbina.getStavke()));


        return retVal;
    }

    public List<PorudzbinaOcjenjivanjeDto> convert(List<Porudzbina> source){
        List<PorudzbinaOcjenjivanjeDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Porudzbina a = (Porudzbina) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
