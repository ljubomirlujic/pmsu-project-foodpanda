package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.model.Stavka;
import com.ftn.foodpanda.service.ArtikalService;
import com.ftn.foodpanda.service.StavkaService;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.StavkaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class StavkeDtoToStavke implements Converter<StavkaDto, Stavka>{

    @Autowired
    private StavkaService stavkaService;

    @Autowired
    private ArtikalService artikalService;

    @Override
    public Stavka convert(StavkaDto stavkaDto) {

        Stavka stavka = null;
        if(stavkaDto.getId() != null){
            stavka = (Stavka)this.stavkaService.findOne(stavkaDto.getId()).get();
        }

        if(stavka == null){
            stavka = new Stavka();
        }

        stavka.setKolicina(stavkaDto.getKolicina());
        Artikal artikal = artikalService.findOne(stavkaDto.getArtikal().getId()).get();
        stavka.setArtikal(artikal);

        return stavka;
    }

    public List<Stavka> convert(List<StavkaDto> source){
        List<Stavka> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            StavkaDto a = (StavkaDto) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
