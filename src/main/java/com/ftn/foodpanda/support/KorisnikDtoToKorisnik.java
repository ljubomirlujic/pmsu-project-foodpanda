package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.service.KorisnikService;
import com.ftn.foodpanda.web.dto.KorisnikDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KorisnikDtoToKorisnik implements Converter<KorisnikDto, Korisnik> {

    @Autowired
    private KorisnikService korisnikService;

    @Override
    public Korisnik convert(KorisnikDto korisnikDto) {

        Korisnik korisnik = null;
        if(korisnikDto.getId() != null){
            korisnik = (Korisnik)this.korisnikService.findOne(korisnikDto.getId()).get();
        }

        if(korisnik == null){
            korisnik = new Korisnik();
        }

        korisnik.setIme(korisnikDto.getIme());
        korisnik.setPrezime(korisnikDto.getPrezime());
        korisnik.setUsername(korisnikDto.getUsername());
        korisnik.setBlokiran(korisnikDto.isBlokiran());

        return korisnik;
    }
}
