package com.ftn.foodpanda.support;


import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import org.springframework.stereotype.Component;

import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ArtikalToArtikalDto implements Converter<Artikal, ArtikalDto> {


    public ArtikalToArtikalDto() {
    }


    @Override
    public ArtikalDto convert(Artikal artikal) {
        ArtikalDto retValue = new ArtikalDto();
        retValue.setId(artikal.getId());
        retValue.setNaziv(artikal.getNaziv());
        retValue.setOpis(artikal.getOpis());
        retValue.setCena(artikal.getCena());
        retValue.setPutanjaSlike(artikal.getPutanjaSlike());
        retValue.setProdavacId(artikal.getProdavacId());
        return retValue;
    }

    public List<ArtikalDto> convert(List<Artikal> source){
        List<ArtikalDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Artikal a = (Artikal) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
