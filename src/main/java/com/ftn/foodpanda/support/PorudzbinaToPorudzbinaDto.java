package com.ftn.foodpanda.support;


import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.web.dto.PorudzbinaDto;
import com.ftn.foodpanda.web.dto.ProdavacOcjenaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class PorudzbinaToPorudzbinaDto implements Converter<Porudzbina, PorudzbinaDto> {

    @Autowired
    private StavkeToDto stavkeToDto;

    @Autowired
    private KupacToKupacDto kupacToKupacDto;

    @Override
    public PorudzbinaDto convert(Porudzbina porudzbina) {

        PorudzbinaDto retVal = new PorudzbinaDto();
        retVal.setId(porudzbina.getId());
        retVal.setAnonimanKomentar(porudzbina.isAnonimanKomentar());
        retVal.setDostavljeno(porudzbina.isDostavljeno());
        retVal.setKomentar(porudzbina.getKomentar());
        retVal.setArhiviranKomentar(porudzbina.isArhiviranKomentar());
        retVal.setSatnica(porudzbina.getSatnica());
        retVal.setOcena(porudzbina.getOcena());
        retVal.setStavke(stavkeToDto.convert(porudzbina.getStavke()));
        retVal.setKupac(kupacToKupacDto.convert(porudzbina.getKupac()));


        return retVal;
    }

    public List<PorudzbinaDto> convert(List<Porudzbina> source){
        List<PorudzbinaDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Porudzbina a = (Porudzbina) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
