package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.web.dto.ArtikalPorudzbinaDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ArtikalToArtikalPorudzbinaDto implements Converter<Artikal, ArtikalPorudzbinaDto> {


    @Override
    public ArtikalPorudzbinaDto convert(Artikal artikal) {

        ArtikalPorudzbinaDto retVal = new ArtikalPorudzbinaDto();
        retVal.setId(artikal.getId());
        retVal.setNaziv(artikal.getNaziv());
        artikal.setCena(artikal.getCena());
        artikal.setPutanjaSlike(artikal.getPutanjaSlike());
        artikal.setOpis(artikal.getOpis());
        artikal.setProdavacId(artikal.getProdavacId());


        return retVal;
    }
}
