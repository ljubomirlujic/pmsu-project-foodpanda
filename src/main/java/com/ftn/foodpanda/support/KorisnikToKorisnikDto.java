package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Artikal;
import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.web.dto.ArtikalDto;
import com.ftn.foodpanda.web.dto.KorisnikDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class KorisnikToKorisnikDto implements Converter<Korisnik, KorisnikDto> {


    @Override
    public KorisnikDto convert(Korisnik korisnik) {

        KorisnikDto retVal = new KorisnikDto();
        retVal.setId(korisnik.getId());
        retVal.setIme(korisnik.getIme());
        retVal.setPrezime(korisnik.getPrezime());
        retVal.setUsername(korisnik.getUsername());
        retVal.setBlokiran(korisnik.isBlokiran());

        return retVal;
    }

    public List<KorisnikDto> convert(List<Korisnik> source){
        List<KorisnikDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Korisnik a = (Korisnik) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
