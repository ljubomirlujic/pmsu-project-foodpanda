package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Porudzbina;
import com.ftn.foodpanda.service.PorudzbinaService;
import com.ftn.foodpanda.web.dto.PorudzbinaOcjenjivanjeDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PorudzbinaOcjenjivanjeDtoToEntity implements Converter<PorudzbinaOcjenjivanjeDto, Porudzbina> {




    private final StavkeDtoToStavke stavkeDtoToStavke;

    public PorudzbinaOcjenjivanjeDtoToEntity(StavkeDtoToStavke stavkeDtoToStavke) {
        this.stavkeDtoToStavke = stavkeDtoToStavke;
    }

    @Override
    public Porudzbina convert(PorudzbinaOcjenjivanjeDto porudzbinaOcjenjivanjeDto) {


        Porudzbina porudzbina = new Porudzbina();


        porudzbina.setSatnica(porudzbinaOcjenjivanjeDto.getSatnica());
        porudzbina.setDostavljeno(porudzbinaOcjenjivanjeDto.isDostavljeno());
        porudzbina.setAnonimanKomentar(porudzbinaOcjenjivanjeDto.isAnonimanKomentar());
        porudzbina.setArhiviranKomentar(porudzbinaOcjenjivanjeDto.isArhiviranKomentar());
        porudzbina.setKomentar(porudzbinaOcjenjivanjeDto.getKomentar());
        porudzbina.setOcena(porudzbinaOcjenjivanjeDto.getOcena());
        porudzbina.setStavke(stavkeDtoToStavke.convert(porudzbinaOcjenjivanjeDto.getStavke()));

        return porudzbina;
    }
}
