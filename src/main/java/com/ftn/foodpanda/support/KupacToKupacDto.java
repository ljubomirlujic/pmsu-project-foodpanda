package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Kupac;
import com.ftn.foodpanda.web.dto.KupacDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class KupacToKupacDto implements Converter<Kupac, KupacDto> {

    @Autowired
    private KorisnikToKorisnikDto korisnikToKorisnikDto;

    @Override
    public KupacDto convert(Kupac kupac) {
        KupacDto retVal = new KupacDto();
        retVal.setId(kupac.getId());
        retVal.setAdresa(kupac.getAdresa());
        retVal.setKorisnik(korisnikToKorisnikDto.convert(kupac.getKorisnik()));

        return retVal;
    }
}
