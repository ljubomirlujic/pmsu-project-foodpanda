package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.service.PorudzbinaService;
import com.ftn.foodpanda.web.dto.ProdavacDto;
import com.ftn.foodpanda.web.dto.ProdavacOcjenaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
@Component
public class ProdavacToProdavacOcjenaDto implements Converter<Prodavac, ProdavacOcjenaDto> {
    @Autowired
    private KorisnikToKorisnikDto korisnikToKorisnikDto;

    @Autowired
    private PorudzbinaService porudzbinaService;

    @Override
    public ProdavacOcjenaDto convert(Prodavac prodavac) {

        ProdavacOcjenaDto retVal = new ProdavacOcjenaDto();
        retVal.setId(prodavac.getId());
        retVal.setAdresa(prodavac.getAdresa());
        retVal.setNaziv(prodavac.getNaziv());
        retVal.setEmail(prodavac.getEmail());
        retVal.setPoslujeOd(prodavac.getPoslujeOd());
        retVal.setKorisnik(korisnikToKorisnikDto.convert(prodavac.getKorisnik()));
        retVal.setProsjecnaOcjena(porudzbinaService.getProsjecnaOcjenaProdavca(prodavac.getId()));

        return retVal;
    }


    public List<ProdavacOcjenaDto> convert(List<Prodavac> source){
        List<ProdavacOcjenaDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Prodavac a = (Prodavac) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
