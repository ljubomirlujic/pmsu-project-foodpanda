package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Korisnik;
import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.service.PorudzbinaService;
import com.ftn.foodpanda.web.dto.KorisnikDto;
import com.ftn.foodpanda.web.dto.ProdavacDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class ProdavacToProdavacDto implements Converter<Prodavac, ProdavacDto> {

    @Autowired
    private KorisnikToKorisnikDto korisnikToKorisnikDto;

    @Autowired
    private PorudzbinaService porudzbinaService;

    @Override
    public ProdavacDto convert(Prodavac prodavac) {

        ProdavacDto retVal = new ProdavacDto();
        retVal.setId(prodavac.getId());
        retVal.setAdresa(prodavac.getAdresa());
        retVal.setNaziv(prodavac.getNaziv());
        retVal.setEmail(prodavac.getEmail());
        retVal.setPoslujeOd(prodavac.getPoslujeOd());
        retVal.setKorisnik(korisnikToKorisnikDto.convert(prodavac.getKorisnik()));

        return retVal;
    }


    public List<ProdavacDto> convert(List<Prodavac> source){
        List<ProdavacDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Prodavac a = (Prodavac) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }
}
