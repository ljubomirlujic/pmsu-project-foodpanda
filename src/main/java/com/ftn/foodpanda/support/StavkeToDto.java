package com.ftn.foodpanda.support;

import com.ftn.foodpanda.model.Prodavac;
import com.ftn.foodpanda.model.Stavka;
import com.ftn.foodpanda.web.dto.ProdavacOcjenaDto;
import com.ftn.foodpanda.web.dto.StavkaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class StavkeToDto implements Converter<Stavka, StavkaDto> {


    @Autowired
    private ArtikalToArtikalPorudzbinaDto toArtikalPorudzbinaDto;

    @Override
    public StavkaDto convert(Stavka stavka) {

        StavkaDto retVal = new StavkaDto();
        retVal.setId(stavka.getId());
        retVal.setArtikal(toArtikalPorudzbinaDto.convert(stavka.getArtikal()));
        retVal.setKolicina(stavka.getKolicina());

        return retVal;
    }

    public List<StavkaDto> convert(List<Stavka> source){
        List<StavkaDto> ret = new ArrayList();
        Iterator var3 = source.iterator();

        while (var3.hasNext()){
            Stavka a = (Stavka) var3.next();
            ret.add(this.convert(a));
        }
        return ret;
    }

}
