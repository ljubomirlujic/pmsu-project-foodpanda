package com.ftn.foodpanda.enumerations;

import javax.persistence.Enumerated;

public enum KorisnickaUloga {

    KUPAC,
    PRODAVAC,
    ADMINISTRATOR
}
