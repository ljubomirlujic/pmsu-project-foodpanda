package com.ftn.foodpanda.esModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

@Getter
@Setter
@AllArgsConstructor
@Builder
@Document(indexName = "artikli")
@Setting(settingPath = "/analyzers/serbianAnalyzer.json")
public class ArtikalEs {

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String naziv;

    @Field(type = FieldType.Text)
    private String opis;

    @Field(type = FieldType.Double)
    private double cena;

    @Field(type = FieldType.Double)
    private double prosjecnaOcjena;

    @Field(type = FieldType.Integer)
    private int brojKomentara;
}
