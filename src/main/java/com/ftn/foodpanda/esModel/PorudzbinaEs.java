package com.ftn.foodpanda.esModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.time.LocalDateTime;


@Getter
@Setter
@AllArgsConstructor
@Builder
@Document(indexName = "porudzbine")
@Setting(settingPath = "/analyzers/serbianAnalyzer.json")
public class PorudzbinaEs {

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String komentar;

    @Field(type = FieldType.Integer)
    private int ocena;

    @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
    private LocalDateTime satnica;

    @Field(type = FieldType.Boolean)
    private boolean anonimanKomentar;

}
