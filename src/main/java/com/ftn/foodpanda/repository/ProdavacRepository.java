package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.model.Prodavac;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProdavacRepository extends JpaRepository<Prodavac, Long> {

    Prodavac findFirstByNaziv(String naziv);

}
