package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.model.Porudzbina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PorudzbinaRepository extends JpaRepository<Porudzbina, Long> {


    @Query(value = "select * from porudzbina where kupac_id = ?",
            nativeQuery = true)
    List<Porudzbina> getAllByKupac(Long id);

    @Query(value = "select * from porudzbina where porudzbina.porudzbina_id in" +
            "(select stavka.porudzbina_id from stavka where artikal_id in" +
            "(select id from artikal where prodavac_id = ?)) and ocena > 0",
            nativeQuery = true)
    List<Porudzbina> getAllByProdavac(Long id);



    @Query(value = "select IFNULL(AVG(ocena),0) from porudzbina where porudzbina.porudzbina_id in\n" +
            "    (select stavka.porudzbina_id from stavka where artikal_id in\n" +
            "    (select id from artikal where prodavac_id = ?)) and ocena > 0",
            nativeQuery = true)
    double prosjecnaOcjena(Long id);
}
