package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.model.Stavka;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StavkaRepository extends JpaRepository<Stavka, Long> {
}
