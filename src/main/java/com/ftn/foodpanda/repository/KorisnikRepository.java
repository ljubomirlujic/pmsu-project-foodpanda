package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.model.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

    Korisnik findFirstByUsername(String username);

    Korisnik findFirstByUsernameAndPassword(String username, String password);
}
