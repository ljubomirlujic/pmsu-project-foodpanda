package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.model.Artikal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtikalRepository extends JpaRepository<Artikal, Long> {

    @Query(value = "SELECT * from artikal where prodavac_id = ?",
            nativeQuery = true)
    List<Artikal> findAllByProdavacId(Long id);
}
