package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.esModel.PorudzbinaEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ESPorudzbinaRepository extends ElasticsearchRepository<PorudzbinaEs, String> {

    List<PorudzbinaEs> findAllByKomentar(String komentar);
}
