package com.ftn.foodpanda.repository;

import com.ftn.foodpanda.esModel.ArtikalEs;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ESArtikalRepository extends ElasticsearchRepository<ArtikalEs, String> {

    List<ArtikalEs> findAllByNaziv(String naziv);

    List<ArtikalEs> findAllByOpis(String opis);
}
