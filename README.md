# Project topic
Food ordering

# About
Faculty project, backend implementation of food ordering application, keeping records about users, restaurants, foods, comments, rating, shopping cart, food ordering functionality, leaving cooments about orders, rating restaurants, adding food, search using Elasticsearch...

Frontend link: https://gitlab.com/ljubomirlujic/pmsu-project

# Technologies used
Java, Spring Boot, MySQL, REST, Elasticsearch
